# Echo Demodulator

This is a small tool developed to digitally down-convert time traces recorded by a home-built electron spin resonance spectrometer. The measurement data is stored in National Instrument TDMS Files. 

![Measurement files can be comfortably loaded](/docs/load_file.png?raw=true "Load File Dialog")

![The main window displaying the downconverted signals.](/docs/main_window.png?raw=true "Main Window")