# -*- coding: utf-8 -*-
"""
This file is part of the Echo Demodulator program.
Copyright (c) 2017-2019 by Stefan Weichselbaumer

Echo Demodulator is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Echo Demodulator is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Echo Demodulator.  If not, see <http://www.gnu.org/licenses/>.


The classes defined in this module can be used to demodulate time-traces
from the Millikelvin pulsed EPR setup.
"""
import itertools
import numpy as np
from scipy import optimize, signal

import matplotlib.pyplot as plt

from nptdms import TdmsFile
from lmfit.models import GaussianModel

from echo_demodulator.utils import lowpass

DEMOD_OPTEACH = 1 # Optimize phase for each trace
DEMOD_TRACE   = 2 # Extract optimized phase from single trace
DEMOD_FIXED   = 3 # Use fixed phase for all traces

class DemodulationError(Exception):
    def __init__(self, message, trace):
        super().__init__(message)
        self.message = message
        self.trace = trace

class LoadTraceError(Exception):
    def __init__(self, message):
        super().__init__(message)
        self.message = message

class Measurement(object):
    """Load a TDMS measurement and extract time traces from it."""

    def __init__(self):
        self.datachannels = {}
        self.sweepchannels = []
        # sweepvars is a list of numpy arrays, corresponding to each sweep
        # variable
        self.sweepvars = []

        # Demodulation options
        self.demod_type = None
        self.fixed_trace = 0
        self.fixed_phase = 0.0
        self.if_frequency = 0
        # Apply lowpass filter
        self.apply_lowpass = False
        self.lowpass_frequency = 0
        # Signal decimation after lowpass
        self.decimate = False
        # Optimization ROI
        self.roi = None

        self.traces = []

    @property
    def no_sweeps(self):
        """Returns the number of sweeps."""
        return self.sweepvars[1]

    def get_trace(self, idx):
        #for trace in self.traces:
        #    print(trace.idx, '==', idx, '=', trace.idx == idx)
        #return None
        #if len(self.traces) == 1:
        #    return self.traces[0]
        return next((trace for trace in self.traces if trace.idx == idx), None)

    def load_traces(self, tdms, sweepchannels, datachannels):
        self.sweepchannels = sweepchannels
        self.datachannels = datachannels

        have_temp = datachannels['temperature'] is not None

        T = tdms.object(*datachannels['time']).data
        I = tdms.object(*datachannels['inphase']).data
        Q = tdms.object(*datachannels['quadrature']).data

        for group, channel in self.sweepchannels:
            data = tdms.object(group, channel).data
            s, indices = np.unique(data, return_index=True)
            sweep = data[np.sort(indices)]
            self.sweepvars.append(sweep)

        datapoints = int(T.shape[0] / np.prod([sv.shape[0] for sv in self.sweepvars]))
        newshape = [sv.shape[0] for sv in self.sweepvars] + [datapoints]

        try:
            I.shape = Q.shape = newshape
        except ValueError:
            raise LoadTraceError("I and Q column length do not match sweep variables.")

        T = T[:datapoints]

        if have_temp:
            try:
                Temperature = tdms.object(*datachannels['temperature']).data
                Temperature.shape = newshape[:-1]
            except AttributeError:
                raise LoadTraceError("Temperature column does not match I and Q"
                                     " columns.")

        indexlist = itertools.product(*[range(len(s)) for s in self.sweepvars])
        for idx in indexlist:
            trace = self._create_trace(T, I[idx], Q[idx], idx)
            for i, sweepvaridx in enumerate(idx):
                var = '/'.join(self.sweepchannels[i])
                trace.sweepvars[var] = float(self.sweepvars[i][sweepvaridx])
            trace.idx = idx

            if have_temp:
                trace.attrs['Temperature'] = Temperature[idx]
            else:
                trace.attrs['Temperature'] = 0
            self.traces.append(trace)

    def _create_trace(self, T, I, Q, idx):
        """Creates a trace and sets demodulation parameters."""
        tr = TimeTrace(T, I, Q)
        tr.if_frequency      = self.if_frequency
        tr.apply_lowpass     = self.apply_lowpass
        tr.lowpass_frequency = self.lowpass_frequency
        tr.decimate          = self.decimate
        tr.roi               = self.roi
        tr.index = idx
        return tr


class TimeTrace(object):
    """Represents a single time trace."""

    def __init__(self, T, I, Q, **attrs):
        self.T = T
        self.raw_T = self.T.copy()
        # complex signal
        self.Z = I - 1.0j*Q
        self.raw_Z = self.Z.copy()

        # attrs contains attributes like temperature etc.
        self.attrs = attrs
        # sweepvars 
        self.sweepvars = {}

        # Sample rate of the signal
        self.fs = 1/np.abs(T[0] - T[1])

        # Intermediate frequency
        self.if_frequency = 0
        # Apply lowpass filter
        self.apply_lowpass = False
        self.lowpass_frequency = 0
        # Signal decimation after lowpass
        self.decimate = False
        # Sample rate after decimation and decimation factor
        self.decimated_fs = 0
        self.decimation_factor = 1

        self.demod_phase = 0.0

        self.roi = None
        self.index = None


    def __repr__(self):
        return "<TimeTrace({},{})>".format(
                ','.join('{}={}'.format(k, v) for k, v in self.sweepvars.items()),
                ','.join('{}={}'.format(k, v) for k, v in self.attrs.items()))

    def _demodulation_helper(self, T, Z, if_frequency, phase,
                             apply_lowpass, lowpass_frequency=None):
        Zd = Z * np.exp(-1j*(2*np.pi*if_frequency*T + phase)) 
        if apply_lowpass:
            Zd = lowpass(Zd, lowpass_frequency, self.fs)
        return Zd

    def demodulate(self, phase, **kwargs):
        """Demodulate the trace.

        The demodulated signal is saved in `self.Zdemod`. kwargs can be used
        to override parameters:

            if_frequency : The if_frequency used
            apply_lowpass : Apply lowpass after downconversion
            lowpass_frequency : Cutoff frequency of lowpass filter
            decimate : Decimate signal after downconversion & lowpass
        """
        if_frequency      = kwargs.get('if_frequency', self.if_frequency)
        apply_lowpass     = kwargs.get('apply_lowpass', self.apply_lowpass)
        lowpass_frequency = kwargs.get('lowpass_frequency', self.lowpass_frequency)
        decimate          = kwargs.get('decimate', self.decimate)

        self.demod_phase = phase
        Z = self._demodulation_helper(self.raw_T, self.raw_Z, if_frequency, phase,
                                      apply_lowpass, lowpass_frequency)
        if decimate:
            # Calculate decimation factor by required bandwidth
            required_bw = 2*lowpass_frequency
            factor = int(np.round(self.fs / required_bw))
            self.decimated_fs = self.fs / factor
            self.decimation_factor = factor
        else:
            factor = 1
        self.Z = Z[::factor]
        self.T = self.raw_T[::factor]

    def demodulate_optimize(self, roi=None, **kwargs):
        """Optimizes the demodulation phase.
        
        The demodulation phase is optimized by maximizing the integrated echo
        area of the in-phase (or quadrature) component. The roi parameter can
        be used to specify the integration window. Further supported
        parameters are:
            
            if_frequency : The if frequency of the signal
            apply_lowpass : Apply lowpass before integration
            lowpass_frequency : Cutoff frequency of lowpass filter
            component : Component to use for integration ('real' or
                        'imaginary', default: 'real')
            """
        def optimization_helper(phase, T, Z, if_frequency, apply_lowpass,
                                lowpass_frequency, component):
            Zd = self._demodulation_helper(T, Z, if_frequency, phase,
                                          apply_lowpass, lowpass_frequency)
            return (np.sum(np.power(np.imag(Zd), 2)) -
                        np.sum(np.power(np.real(Zd), 2)))

        if_frequency      = kwargs.get('if_frequency', self.if_frequency)
        apply_lowpass     = kwargs.get('apply_lowpass', self.apply_lowpass)
        lowpass_frequency = kwargs.get('lowpass_frequency', self.lowpass_frequency)
        component = getattr(np, kwargs.get('component', 'real'))
        start_phase       = kwargs.get('start_phase', np.pi/2)
        roi               = kwargs.get('roi', self.roi)

        if not roi:
            roi = slice(None)

        Z = self.raw_Z[roi].copy()
        T = self.raw_T[roi].copy()

        result = optimize.minimize(optimization_helper, (start_phase,),
                args=(T, Z, if_frequency, apply_lowpass, lowpass_frequency,
                      component), method='SLSQP',
                bounds=((0, 2*np.pi),),
                options=dict(disp=False))

        if result.success:
            phase = result.x[0]
            self.demodulate(phase, **kwargs)
            # If the echo signal in I is negative, we need a 180 degree phase
            # shift. We apply this only to the ROI.
            I = np.real(self.Z[roi])
            if np.abs(I.min()) > np.abs(I.max()):
                phase += np.pi
                if phase > 2*np.pi:
                    phase -= 2*np.pi
            self.demodulate(phase, **kwargs)
            return phase
        else:
            raise DemodulationError("Optimization did not converge.", self)

    def integrate(self, roi=None, apply_window=True, component=np.real):
        roi = roi or self.roi
        if not roi:
            roi = slice(None)
        Z = component(self.Z[roi])
        T = self.T[roi]
        return self._integration_helper(T, Z, apply_window, component)

    def _integration_helper(self, T, Z, apply_window=True,
                            component=np.real):
        """Integrates the time trace."""
        Z = component(Z)
        if apply_window:
            # estimate the position and width of the echo using a Gaussian 
            model = GaussianModel()
            params = model.guess(Z, x=T)
            result = model.fit(Z, params, x=T)
            # The halfwidth of the window is 1.5 to 2 times the FWHM of the
            # Gaussian
            halfwidth = int(1.7*result.params['fwhm'] * self.fs)
            center = int(result.params['center'] * self.fs)
            window = signal.tukey(2*halfwidth)
            window_padded = np.zeros_like(T)
            window_padded[center-halfwidth:center+halfwidth] = window
            Z = Z * window_padded
        return np.trapz(Z, x=T)

if __name__ == '__main__':
    tdms = \
    TdmsFile(r'M:\2017-06-11-Session_8_P2cluster\Measurements\070g_2017-08-31_50mK_ResC_DegeneracyPoint_BSweep.tdms')
    sweepvalues = tdms.channel_data('Read.M4i', 'IPS120.target field (T)')
    T = tdms.channel_data('Read.M4i', 'Time')
    I = tdms.channel_data('Read.M4i', 'Channel0')
    Q = tdms.channel_data('Read.M4i', 'Channel1')

    newshape = (sweepvalues.shape[0],
                int(I.shape[0]/sweepvalues.shape[0]))
    T.shape = I.shape = Q.shape = newshape
    T = T[0]

    from tqdm import tqdm

    import tables
    from mytypes import DemodulatedTimeTraceInfo

    h5file = tables.open_file("demodulated-test.hdf5", mode="w")
    info = h5file.create_table(h5file.root, 'info', 
            DemodulatedTimeTraceInfo, "Information about time trace")

    idx = 8
    tt = TimeTrace(T, I[idx], Q[idx])
    roi = slice(0, np.abs(tt.T - 40e-6).argmin())
    phase = tt.demodulate_optimize(42.5e6, roi=roi)
    tt.demodulate(42.5e6, phase, lowpass_frequency=10e6)

    time_storage = h5file.create_array(h5file.root, 'time', tt.T,
                                        title='Time information')
    filters = tables.Filters(complevel=5, complib='blosc')
    signal_storage = h5file.create_earray(h5file.root, 'signal',
                                          atom=tables.Atom.from_dtype(tt.Z.dtype),
                                          shape=(0, tt.Z.shape[0]),
                                          expectedrows=sweepvalues.shape[0],
                                          title='Complex demodulated signal',
                                          filters=filters)

    echo = info.row
    for idx in tqdm(range(sweepvalues.shape[0])):
        tt = TimeTrace(T, I[idx], Q[idx])
        phase = tt.demodulate_optimize(42.5e6, roi=roi)
        tt.demodulate(42.5e6, phase, lowpass_frequency=10e6)
        echo['demod_phase'] = phase
        echo['phase_optimized'] = True
        echo['fsample_original'] = 500e6
        echo['fsample'] = tt.fs
        echo['decimation_factor'] = 25
        echo['magnetic_field'] = sweepvalues[idx]
        echo['tau'] = 80e-6
        echo['pi_amplitude'] = 1.0
        echo['data_index'] = idx
        echo.append()

        signal_storage.append(np.reshape(tt.Z, (1, tt.Z.shape[0])))


    info.flush()
    h5file.close()


    #def exp_decay(x, A, T2):
    #    return A*np.exp(-x/T2)
    #from lmfit.models import Model
    #model = Model(exp_decay)
    #params = model.make_params(T2=1e-3, A=1e-7)
    #result = model.fit(phases, params, x=2*sweepvalues)
    #print(result.fit_report())

    #result.plot(datafmt='o', initfmt='', data_kws=dict(mew=1, mfc='none'))
    #plt.tight_layout()
    #plt.show()
