# -*- coding: utf-8 -*-
"""
This file is part of the Echo Demodulator program.
Copyright (c) 2017-2019 by Stefan Weichselbaumer

Echo Demodulator is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Echo Demodulator is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Echo Demodulator.  If not, see <http://www.gnu.org/licenses/>.


This module defines the LoadFileDialog class. This dialog allows to comfortably
load TDMS files of the Millikelvin pulsed ESR spectrometer.
"""
import os
import sys 
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.uic import loadUi

import numpy as np
from nptdms import TdmsFile

from echo_demodulator.mpl import MplCanvas, MplNavigationToolbar, MeasurementPlotter
import echo_demodulator.demodulation as dm

class LoadFileDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(LoadFileDialog, self).__init__(parent)

        ui_file = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               'ui', 'load_file.ui')
        loadUi(ui_file, self)

        self.statusBar = QtWidgets.QStatusBar(self)
        self.statusBarLayout.addWidget(self.statusBar)
        self.statusBar.showMessage("Select TDMS file to continue")

        self.plotter = MeasurementPlotter(self)
        self.layoutPreview.addWidget(self.plotter)

        self.buttonBrowsePath.clicked.connect(self.onBrowsePath)
        self.buttonPlotPreview.clicked.connect(self.onPlotPreview)
        self.comboTdmsGroup.currentIndexChanged[str].connect(self.onTdmsGroupSelected)
        self.comboTempGroup.currentIndexChanged[str].connect(self.onTempGroupSelected)

        self.textDemodTrace.cursorPositionChanged.connect(self.onDemodTraceClicked)
        self.textDemodPhase.cursorPositionChanged.connect(self.onDemodPhaseClicked)

        self.dataCombos = [self.comboTimeChannel, self.comboInPhaseChannel,
                           self.comboQuadratureChannel]
        self.sweepCombos = [self.comboSweepChannel1, self.comboSweepChannel2]
        self.buttonBox.accepted.connect(self.onDialogAccepted)

        self.measurement = None

        self.restoreDefaults()
        self.onBrowsePath(False)

    def done(self, result):
        if result == QtWidgets.QDialog.Accepted:
            try:
                self.getMeasurement()
            except dm.LoadTraceError as e:
                QtWidgets.QMessageBox.critical(self,
                    "done() Error when loading measurement",
                    "done() Could not load traces:\n\n{}".format(e))
                return
            super(LoadFileDialog, self).done(QtWidgets.QDialog.Accepted)
        else:
            super(LoadFileDialog, self).done(result)

    def onDialogAccepted(self):
        """Save the last used settings."""
        settings = QtCore.QSettings("WMI", "Echo Demodulator")

        settings.setValue("radioOptimizeEach", self.radioOptimizeEach.isChecked())
        settings.setValue("radioExtractTrace", self.radioExtractTrace.isChecked())
        settings.setValue("radioFixedPhase", self.radioFixedPhase.isChecked())

        settings.setValue("textDemodTrace", self.textDemodTrace.text())
        settings.setValue("textDemodPhase", self.textDemodPhase.text())
        settings.setValue("checkDecimate", self.checkDecimate.isChecked())
        settings.setValue("checkApplyLowpass", self.checkApplyLowpass.isChecked())
        settings.setValue("textLowpassFrequency", self.textLowpassFrequency.text())
        settings.setValue("textIfFrequency", self.textIfFrequency.text())
        settings.sync()

    def restoreDefaults(self):
        settings = QtCore.QSettings("WMI", "Echo Demodulator")
        self.checkDecimate.setChecked(settings.value("checkDecimate", True, type=bool))
        self.checkApplyLowpass.setChecked(settings.value("checkApplyLowpass",
                                          True, type=bool))
        self.textDemodTrace.setText(settings.value("textDemodTrace", ""))
        self.textDemodPhase.setText(settings.value("textDemodPhase", ""))
        self.textLowpassFrequency.setText(settings.value("textLowpassFrequency", "10"))
        self.textIfFrequency.setText(settings.value("textIfFrequency", "62.5"))
        if settings.value("radioOptimizeEach", True, type=bool):
            self.radioOptimizeEach.setChecked(True)
        if settings.value("radioExtractTrace", False, type=bool):
            self.radioExtractTrace.setChecked(True)
        if settings.value("radioFixedPhase", False, type=bool):
            self.radioFixedPhase.setChecked(True)

    def onBrowsePath(self, checked):
        settings = QtCore.QSettings("WMI", "Echo Demodulator")
        last_path = settings.value("last_path", r"")

        file, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Select TDMS file",
                        last_path, "TDMS files (*.tdms)")
        if not file:
            return
        self.textTDMSPath.setText(file)

        last_path = os.path.dirname(os.path.abspath(file))
        settings.setValue("last_path", last_path)
        settings.sync()

        self.openTDMSFile()

    def openTDMSFile(self):
        self.path = self.textTDMSPath.text()
        
        self.statusBar.showMessage("Loading TDMS file...")
        try:
            self.tdms = TdmsFile(self.path)
        except ValueError:
            QtWidgets.QMessageBox.critical(self, "Invalid file",
                    "The file at the specified path is not a valid TDMS file.")
            self.statusBar.showMessage("")
            return

        comment = self.tdms.object().property('Comment')
        self.textComment.setText(comment)
        groups = self.tdms.groups()
        self.comboTdmsGroup.insertItems(0, groups)
        self.comboTempGroup.insertItems(0, groups)
        self.comboTempGroup.insertItem(0, '- None -')
        self.comboTempGroup.setCurrentIndex(0)
        for tdms_group in ['Read.M4i', 'Read.Gage']:
            try:
                self.comboTdmsGroup.setCurrentIndex(groups.index(tdms_group))
            except ValueError as e:
                pass
        for temp_group in ['Read.TRITON', 'Read.Triton']:
            try:
                self.comboTempGroup.setCurrentIndex(groups.index('Read.Triton')+1)
            except ValueError:
                pass
        self.statusBar.showMessage("Select data groups and channels")

    def onPlotPreview(self):
        try:
            self.measurement = None
            measurement = self.getMeasurement()
            self.plotter.setMeasurement(measurement)
        except (AttributeError, dm.LoadTraceError) as e:
            QtWidgets.QMessageBox.critical(self,
                "Error when loading measurement",
                "Could not load traces:\n\n{}".format(e))
            return None


    def _extractChannels(self, channels):
        """Returns data and sweep channels from a list of channels."""
        # We need to separate sweep channels from normal channels here
        data_channels  = []
        sweep_channels = ['- None -']
        for channel in channels:
            # Ignore timestamp channel
            if channel.channel == 'Timestamp':
                continue
            # Sweep channels have DR_DataType of 'Value', while data channels
            # have 1DArray, 2DArray etc.
            datatype = channel.property('DR_DataType')
            if datatype == 'Value':
                sweep_channels.append(channel.channel)
            else:
                data_channels.append(channel.channel)
        return data_channels, sweep_channels

    def onTdmsGroupSelected(self, text):
        channels = self.tdms.group_channels(text)
        data_channels, sweep_channels = self._extractChannels(channels)
        for combo in self.dataCombos:
            combo.clear()
            combo.insertItems(0, data_channels)
        for combo in self.sweepCombos:
            combo.clear()
            combo.insertItems(0, sweep_channels)

        channel_list = [('Time', 'Channel0', 'Channel1'),
                ('Time(sec)', 'Ch1(V)', 'Ch2(V)')]
        for tc, ch1, ch2 in channel_list:
            try:
                self.comboTimeChannel.setCurrentIndex(data_channels.index(tc))
                self.comboInPhaseChannel.setCurrentIndex(data_channels.index(ch1))
                self.comboQuadratureChannel.setCurrentIndex(data_channels.index(ch2))
            except ValueError:
                continue

        if len(sweep_channels) >= 2:
            self.comboSweepChannel1.setCurrentIndex(1)
        if len(sweep_channels) == 3:
            self.comboSweepChannel2.setCurrentIndex(2)

    def onTempGroupSelected(self, text):
        if text == '- None -':
            return

        channels = self.tdms.group_channels(text)
        # We need to separate sweep channels from normal channels here
        channels  = [c.channel for c in channels if c.channel != 'Timestamp']
        self.comboTempChannel.clear()
        self.comboTempChannel.insertItems(0, channels)
        try:
            self.comboTempChannel.setCurrentIndex(
                    channels.index('Sample RuO2 BCD8'))
        except ValueError:
            pass

    def onDemodTraceClicked(self, old, new):
        self.radioExtractTrace.setChecked(True)

    def onDemodPhaseClicked(self, old, new):
        self.radioFixedPhase.setChecked(True)

    def getMeasurement(self):
        if not self.measurement:
            self.measurement = self.getMeasurementInternal()

        return self.measurement

    def getMeasurementInternal(self):
        measurement = dm.Measurement()

        # Demodulation parameters should be set before load_traces is called.
        measurement.if_frequency = float(self.textIfFrequency.text()) * 1e6
        measurement.apply_lowpass = self.checkApplyLowpass.isChecked()
        measurement.lowpass_frequency = float(self.textLowpassFrequency.text()) * 1e6
        measurement.decimate = self.checkDecimate.isChecked()

        button = self.radioDemodOptions.checkedButton()
        name = button.objectName()
        if name == 'radioOptimizeEach':
            measurement.demod_type = dm.DEMOD_OPTEACH
        elif name == 'radioExtractTrace':
            measurement.demod_type = dm.DEMOD_TRACE
            measurement.fixed_trace = int(self.textDemodTrace.text())
        else:
            measurement.demod_type = dm.DEMOD_FIXED
            measurement.fixed_phase = float(self.textDemodPhase.text())

        if self.plotter.roi:
            measurement.roi = self.plotter.roi

        measurement.load_traces(self.tdms, self.getSweepChannels(),
                self.getDataChannels())

        measurement.path = self.path
        return measurement

    def getSweepChannels(self):
        """Returns the two sweep channels."""
        channels = []
        group = self.comboTdmsGroup.currentText()
        for ch in [self.comboSweepChannel1.currentText(),
                self.comboSweepChannel2.currentText()]:
            if not ch == '- None -':
                channels.append((group, ch))
        return channels

    def getDataChannels(self):
        """Returns a dictionary containing the data channel names."""
        group = self.comboTdmsGroup.currentText()
        if self.comboTempGroup.currentText() != '- None -':
            temperature = (self.comboTempGroup.currentText(),
                                 self.comboTempChannel.currentText())
        else:
            temperature = None

        return dict(time=(group, self.comboTimeChannel.currentText()),
                    inphase=(group, self.comboInPhaseChannel.currentText()),
                    quadrature=(group, self.comboQuadratureChannel.currentText()),
                    temperature=temperature)

    def getDemodOptions(self):
        """Return a tuple containing the demodulation options. 
        
        The first item specifies the demodulation type ('each', 'trace',
        'fixed').  The meaning of the second parameter depends on the
        first: for 'each', the second parameter is always None, for
        'trace' it specifies the index of the trace to use, for 'fixed' it
        specifies the phase used for demodulation.
        """
        button = self.radioDemodOptions.checkedButton()
        name = button.objectName()
        if name == 'radioOptimizeEach':
            return ('each', None)
        elif name == 'radioExtractTrace':
            return ('trace', int(self.textDemodTrace.text()))
        else:
            return ('fixed', float(self.textDemodPhase.text()))

