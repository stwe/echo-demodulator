# -*- coding: utf-8 -*-
"""
This file is part of the Echo Demodulator program.
Copyright (c) 2017-2019 by Stefan Weichselbaumer

Echo Demodulator is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Echo Demodulator is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Echo Demodulator.  If not, see <http://www.gnu.org/licenses/>.


This module contains the main application logic.
"""
import os
import pathlib
import re
import sys 
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.uic import loadUi

import numpy as np
import matplotlib as mpl
from matplotlib.figure import Figure
import tables

#from mpl import MplCanvas
import echo_demodulator.demodulation as dm
from echo_demodulator.load_file import LoadFileDialog
from echo_demodulator.mpl import MeasurementPlotter
from echo_demodulator.mytypes import DemodulatedTimeTraceInfo, from_trace
from echo_demodulator.utils import timescale

try:
    import tables
except ImportError:
    tables = None

MAX_DEMOD_TRIES = 3

class DemodulationSignals(QtCore.QObject):
    # Emitted when the demodulation is finished
    finished = QtCore.pyqtSignal()
    error    = QtCore.pyqtSignal(object)

class DemodulationJob(QtCore.QRunnable):
    def __init__(self, trace, **kwargs):
        super().__init__()
        self.trace = trace
        self.kwargs = kwargs
        self.signals = DemodulationSignals()

    def run(self):
        demod_type        = self.kwargs['demod_type']
        phase             = self.kwargs.get('phase', 0.0)
        roi               = self.kwargs.get('roi', None)

        try:
            if demod_type == dm.DEMOD_OPTEACH:
                phase = self.trace.demodulate_optimize(roi)
            else:
                self.trace.demodulate(phase)
            self.signals.finished.emit()
        except dm.DemodulationError as e:
            self.signals.error.emit(e)


class MainWindow(QtWidgets.QMainWindow):
    # Signal called when demodulation of a trace is finished
    demodulation_finished = QtCore.pyqtSignal()

    def __init__(self):
        super(MainWindow, self).__init__()

        ui_file = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               'ui', 'main.ui')
        loadUi(ui_file, self)

        self.layoutOptionsButtons.setAlignment(QtCore.Qt.AlignRight)
        self.layoutIntegratorButtons.setAlignment(QtCore.Qt.AlignRight)
        self.layoutTraceButtons.setAlignment(QtCore.Qt.AlignRight)
        self.layoutIntegrate.setAlignment(QtCore.Qt.AlignTop)

        self.buttonLoadTdmsFile.clicked.connect(self.onLoadTdmsFile)
        self.demodulation_finished.connect(self.onDemodFinished)

        self.buttonDemodAgain.clicked.connect(self.onDemodAgain)
        self.buttonPlotPhases.clicked.connect(self.onPlotDemodPhases)
        self.buttonSaveDemodTraces.clicked.connect(self.onSaveDemodTraces)
        self.textLog.anchorClicked.connect(self.onLogAnchorClicked)
        self.buttonIntegrateAll.clicked.connect(self.onIntegrateEchos)
        self.buttonDrawWindow.clicked.connect(self.onDrawWindow)

        self.plotter = MeasurementPlotter(self)
        self.layoutPlotter.addWidget(self.plotter)
        self.plotter.traceChanged.connect(self.onTraceChanged)
        self.plotter.roiSelected.connect(self.onRoiSelected)

        self.threadpool = QtCore.QThreadPool.globalInstance()
        self.threadpool.setMaxThreadCount(8)
        self.abort = False

        self.currentTrace = None
        self.textLog.setHtml('')

    def closeEvent(self, event):
        self.threadpool.waitForDone(500)
        event.accept()

    def appendLog(self, message):
        text = self.textLog.toHtml()
        text += '<p style="margin: 0;">{}<p/>'.format(message)
        self.textLog.setHtml(text)
        cursor = self.textLog.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        self.textLog.setTextCursor(cursor)

    def onLoadTdmsFile(self):
        lfd = LoadFileDialog(self)
        ret = lfd.exec_()
        if not ret:
            return

        self.measurement = lfd.getMeasurement()
        self.textTdmsPath.setText(lfd.path)
        self.statusBar().showMessage("Loaded {} traces".format(
            len(self.measurement.traces)))
        self.startDemodulationProcess()

    def onDemodAgain(self):
        if self.currentTrace:
            try:
                start_phase = np.random.random() * np.pi
                phase = self.currentTrace.demodulate_optimize(start_phase=start_phase)
                self.currentTrace.demodulate(phase)
                self.plotter.redraw()
            except dm.DemodulationError as e:
                self.appendLog("Demodulation of trace failed: {}".format(e))

    def onPlotDemodPhases(self):
        phases = []
        for trace in self.measurement.traces:
            phases.append(np.rad2deg(trace.demod_phase))
        phases = np.asarray(phases)

        mpl.pyplot.figure("Demodulation phases")
        mpl.pyplot.plot(phases)
        mpl.pyplot.xlabel('Trace number')
        mpl.pyplot.ylabel('Demodulation phase (deg)')
        mpl.pyplot.show()

    def onDrawWindow(self):
        pass

    def onIntegrateEchos(self):
        pass

    def onPlotTypeChanged(self, index):
        self.canvas.drawTrace(self.currentTrace, self.plot_type)

    def onLogAnchorClicked(self, url):
        regex = 'index=(?P<index>[0-9]+(,[0-9]+)*)'
        match = re.match(regex, url.url())
        if match:
            index = list(map(int, match.group('index').split(',')))
            self.plotter.selectTrace(index)

    def onCancelDemodulation(self):
        self.abort = True
        self.threadpool.clear()
        self.demodulation_finished.emit()

    def onDemodFinished(self):
        self.dialog.reset()
        if not self.abort:
            self.statusBar().showMessage("Demodulated {} traces".format(
                len(self.measurement.traces)))
        else:
            self.statusBar().showMessage("Aborted ...")
            return
        self.plotter.setMeasurement(self.measurement)
        self.plotter.redraw()
        self.threadpool.waitForDone()

    def onDemodTraceFinished(self, error=None):
        """Demodulation of a trace has finished. Increase value by one."""
        if self.abort:
            return

        new_value = self.dialog.value() + 1
        if not error:
            self.dialog.setValue(new_value)
        else:
            tries = getattr(error.trace, "tries", 1)
            if tries <= MAX_DEMOD_TRIES:
                setattr(error.trace, "tries", tries+1)
                job = DemodulationJob(error.trace, **self.demod_options)
                job.setAutoDelete(True)
                job.signals.finished.connect(self.onDemodTraceFinished)
                job.signals.error.connect(self.onDemodTraceFinished)
                self.threadpool.start(job)
            else:
                error_template = 'Demodulation of trace <a ' \
                        'href="index={index:s}">{sweepvars:s}</a>' \
                        ' failed.'
                sweepvar_template = '{0:s}={1:g}'
                sweepvarstring = ', '.join(sweepvar_template.format(name, value)
                        for name, value in error.trace.sweepvars.items())
                index = ','.join(str(i) for i in error.trace.index)
                error_message = error_template.format(index=index,
                        sweepvars=sweepvarstring)
                self.appendLog(error_message)
                self.dialog.setValue(new_value)
        if new_value == self.dialog.maximum():
            self.demodulation_finished.emit()

    def onTraceChanged(self, trace):
        if trace:
            self.currentTrace = trace
            self.labelDemodPhase.setText('{:.2f} deg'.format(
                                         np.rad2deg(trace.demod_phase)))
            self.labelOrigSampleRate.setText('{:.0f} MS/s'.format(
                trace.fs / 1e6))
            self.labelCurrentSampleRate.setText('{:.1f} MS/s'.format(
                    trace.decimated_fs / 1e6))
            self.labelDecimationFactor.setText('{:d}'.format(trace.decimation_factor))

    def onRoiSelected(self, roi):
        if roi:
            self.window_center = self.currentTrace.T[int((roi.start + roi.stop)/2)]
            dt = np.abs(self.currentTrace.T[0] - self.currentTrace.T[1]) 
            self.window_width  = np.abs(roi.start - roi.stop) * dt
        else:
            self.window_center = 0
            self.window_width = 0

        rescaled_center, _, unit = timescale(self.window_center)
        self.lineWindowPos.setText('{:.2f} {:s}'.format(rescaled_center, unit))
        rescaled_width, _, unit = timescale(self.window_width)
        self.lineWindowWidth.setText('{:.2f} {:s}'.format(rescaled_width, unit))

    def startDemodulationProcess(self):
        """Start the demodulation process.
        
        If we extract the demod phase from a single trace, we perform this
        task first. Afterwards, we start worker threads which demodulate the
        data.
        """
        if self.measurement.demod_type == dm.DEMOD_TRACE:
            trace = self.measurement.traces[self.measurement.fixed_trace]
            try:
                phase = trace.demodulate_optimize()
            except dm.DemodulationError as e:
                self.appendLog("Demodulation of trace failed: {}".format(e))
        elif self.measurement.demod_type == dm.DEMOD_FIXED:
            phase = np.deg2rad(self.measurement.fixed_phase)
        else:
            phase = 0

        no_traces = len(self.measurement.traces)
        self.dialog = QtWidgets.QProgressDialog("Demodulating {} traces...".format(no_traces),
                "Abort", 1, no_traces, self)
        self.dialog.canceled.connect(self.onCancelDemodulation)
        self.dialog.setWindowTitle("Demodulating...")
        self.dialog.forceShow()

        self.statusBar().showMessage("Demodulating {} traces".format(no_traces))
        self.abort = False
        self.demod_options = dict(demod_type=self.measurement.demod_type,
                                  phase=phase, roi=self.measurement.roi)
        for i, trace in enumerate(self.measurement.traces):
            if self.abort:
                self.threadpool.clear()
                break
            job = DemodulationJob(trace, **self.demod_options)
            job.setAutoDelete(True)
            job.signals.finished.connect(self.onDemodTraceFinished)
            job.signals.error.connect(self.onDemodTraceFinished)
            self.threadpool.start(job)
            QtWidgets.QApplication.processEvents()

    def onSaveDemodTraces(self):
        if not tables:
            QtWidgets.QMessageBox.critical(self, "Save demodulated traces",
                                           "Could not load PyTables")

        target_path = os.path.join(
                os.path.dirname(self.measurement.path),
                '..',
                'Demodulated',
                os.path.basename(self.measurement.path + ".hdf5"))
        #path = pathlib.Path(target_path).resolve()

        fn, _ = QtWidgets.QFileDialog.getSaveFileName(self, 
                "Save traces to file", target_path)
        if not fn:
            return
        fp = tables.File(fn, mode='w')

        descr = from_trace(self.measurement.traces[0])
        table = fp.create_table(fp.root, 'traces', descr,
                                'Demodulated Time Traces')
        row = table.row
        for trace in self.measurement.traces:
            row['fs']                = trace.fs
            row['if_frequency']      = trace.if_frequency
            row['apply_lowpass']     = trace.apply_lowpass
            row['lowpass_frequency'] = trace.lowpass_frequency
            row['decimate']          = trace.decimate
            row['decimation_factor'] = trace.decimation_factor
            row['decimated_fs']      = trace.decimated_fs
            row['demod_phase']       = trace.demod_phase
            row['index']             = trace.index
            if trace.roi:
                row['roi']               = [trace.roi.start, trace.roi.stop]
            items = trace.sweepvars.items()
            row['sweepvars']         = [k for k, v in items]
            row['sweepvalues']       = [v for k, v in items]
            row['T']                 = trace.T
            row['Z']                 = trace.Z
            row.append()

        fp.close()
        self.statusBar().showMessage(
                'Saved demodulated traces to {}'.format(fn))
        
