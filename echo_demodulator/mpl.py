# -*- coding: utf-8 -*-
"""
This file is part of the Echo Demodulator program.
Copyright (c) 2017-2019 by Stefan Weichselbaumer

Echo Demodulator is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Echo Demodulator is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Echo Demodulator.  If not, see <http://www.gnu.org/licenses/>.


The mpl module contains widgets which provide the plotting functionality
for the application.
"""
import os
import numpy as np

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.uic import loadUi
import matplotlib as mpl
from matplotlib.backends.backend_qt5agg import (
        FigureCanvasQTAgg as FigureCanvas, 
        NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure

from echo_demodulator.utils import timescale

mpl.rc('xtick', direction='in', top=True)
mpl.rc('ytick', direction='in', right=True)


class MeasurementPlotter(QtWidgets.QWidget):
    """This widgets plots a single trace of a measurment object.

    It provides a TraceSelector widget to select the sweep variables and
    functionality to select a region of interest (ROI).
    """
    PLOT_IQ       = 0
    PLOT_MAG      = 1

    # This signal is emitted when the currently plotted trace changes
    traceChanged = QtCore.pyqtSignal(object)
    roiSelected = QtCore.pyqtSignal(object)

    def __init__(self, parent=None):
        super().__init__(parent)

        ui_file = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               'ui', 'measurement_plotter.ui')
        loadUi(ui_file, self)

        self.measurement = None
        self.currentTrace = None
        self.roi = None

        self.canvas = MplCanvas(self)
        self.layoutCanvas.addWidget(self.canvas)

        self.toolbar = MplNavigationToolbar(self.canvas, self, coordinates=False)
        self.layoutToolbar.insertWidget(0, self.toolbar)

        self.comboPlotType.currentIndexChanged.connect(self.onPlotTypeChanged)
        self.canvas.roiSelected.connect(self.onRoiSelected)
        self.buttonSelectRoi.clicked.connect(self.onSelectRoi)
        self.buttonDeleteRoi.clicked.connect(self.deleteRoi)

        self.buttonSelectRoi.setEnabled(False)
        self.buttonDeleteRoi.setEnabled(False)

    def resizeEvent(self, event):
        try:
            self.canvas.fig.tight_layout()
        except ValueError:
            pass
        return super().resizeEvent(event)

    @property
    def plot_type(self):
        return self.comboPlotType.currentIndex()

    def setMeasurement(self, measurement):
        self.measurement = measurement
        self.roi = None

        # Create sweep selectors 
        layout = self.groupTraceSelector.layout() or QtWidgets.QVBoxLayout()
        # Delete old sweep selectors
        for i in reversed(range(layout.count())):
            widget = layout.takeAt(i).widget()
            if widget:
                widget.deleteLater()
        # Fill layout with new sweep selectors
        for i, sweepvars in enumerate(self.measurement.sweepvars):
            selector = TraceSelector(sweepvars, self)
            selector.setSweepName('/'.join(self.measurement.sweepchannels[i]))
            selector.valueChanged.connect(self.onTraceSelectorChanged)
            layout.addWidget(selector)
        self.buttonSelectRoi.setEnabled(True)
        self.groupTraceSelector.setLayout(layout)
        self.onTraceSelectorChanged()

    def selectTrace(self, index):
        selector_idxs = range(self.groupTraceSelector.layout().count()) 
        for i, sweep_index in zip(selector_idxs, index):
            selector = self.groupTraceSelector.layout().itemAt(i).widget()
            selector.setSweepIndex(sweep_index)

    def deleteRoi(self):
        self.roi = None
        self.canvas.roi = None
        self.redraw()
        self.roiSelected.emit(self.roi)
        self.buttonDeleteRoi.setEnabled(False)

    def onPlotTypeChanged(self):
        self.canvas.drawTrace(self.currentTrace, self.plot_type)

    def onSelectRoi(self):
        if self.canvas._select_roi:
            self.canvas.select_roi(cancel=True)
            self.buttonSelectRoi.setText('Select ROI')
        else:
            self.buttonDeleteRoi.setEnabled(False)
            self.canvas.select_roi()
            self.buttonSelectRoi.setText('Cancel')

    def onRoiSelected(self, roi):
        self.buttonSelectRoi.setText('Select ROI')
        self.roi = roi
        self.roiSelected.emit(self.roi)
        self.buttonDeleteRoi.setEnabled(True)

    def onTraceSelectorChanged(self):
        layout = self.groupTraceSelector.layout()
        idx = []
        for i in range(layout.count()):
            try:
                item = layout.itemAt(i)
                idx.append(item.widget().sweepIndex())
            except (AttributeError, IndexError):
                pass
        self.currentTrace = self.measurement.get_trace(tuple(idx))
        self.redraw()

    def redraw(self):
        """Redraw the current trace"""
        self.traceChanged.emit(self.currentTrace)
        self.canvas.drawTrace(self.currentTrace, self.plot_type)

class MplCanvas(FigureCanvas):
    roiSelected = QtCore.pyqtSignal(object)

    # Plot type: Either plot in-phase/quadrature or magnitude/phase
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        #self.axes = fig.add_subplot(111)
        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
                QtWidgets.QSizePolicy.Expanding,
                QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.trace = None
        self.timescale = 0

        # If select_roi is True, the user can select a ROI.
        self._select_roi = False
        self._dragging   = False
        # contains the start- and stop line objects
        self.startLine = None
        self.stopLine  = None
        self.roi = []

        self.mpl_connect('button_press_event', self.onButtonPress)
        self.mpl_connect('button_release_event', self.onButtonRelease)
        self.mpl_connect('motion_notify_event', self.onMouseMove)

    def select_roi(self, cancel=False):
        """Starts or resets the ROI selection mode"""
        try:
            idx = self.axes[0].lines.index(self.startLine)
            self.axes[0].lines.pop(idx)
            idx = self.axes[0].lines.index(self.stopLine)
            self.axes[0].lines.pop(idx)
        except (ValueError, AttributeError):
            pass

        if cancel and self._select_roi:
            self._select_roi = False
            self._dragging   = False
            self.roi = None
            self.roiSelected.emit(None)
        else:
            self.roi = [0, 0]
            self._select_roi = True
            self._dragging   = False
        self.redraw()

    def _redrawRoiLine(self, line, x):
        """Redraws the ROI line at a new coordinate."""
        try:
            idx = self.axes[0].lines.index(line)
            self.axes[0].lines.pop(idx)
        except (ValueError, AttributeError):
            pass
        line = self.axes[0].axvline(x=x, ls='--', color='k', lw=1.0)
        self.redraw()
        return line

    def _calculateRoiIndex(self, t):
        """Calculate the index of the specified time."""
        if not self.trace and not self.timescale:
            return 0
        t *= np.power(10, self.timescale)
        return np.abs(self.trace.T - t).argmin()

    def onButtonPress(self, event):
        if self._select_roi:
            self.roi[0] = self._calculateRoiIndex(event.xdata)
            self.startLine = self._redrawRoiLine(self.startLine, event.xdata)
            self._dragging = True

    def onMouseMove(self, event):
        if self._select_roi and self._dragging:
            self.roi[1] = self._calculateRoiIndex(event.xdata)
            self.stopLine = self._redrawRoiLine(self.stopLine, event.xdata)

    def onButtonRelease(self, event):
        if self._select_roi and self._dragging:
            self._dragging = False
            self._select_roi = False
            self.roi[1] = self._calculateRoiIndex(event.xdata)
            self.stopLine = self._redrawRoiLine(self.stopLine, event.xdata)
            self.roiSelected.emit(slice(*self.roi))

    def redraw(self):
        """Refresh the canvas, e.g. after a size change."""
        try:
            # tight_layout fails, if no plot exists
            self.fig.tight_layout(h_pad=0)
        except ValueError:
            pass
        self.draw()

    def drawTrace(self, trace=None, plot_type=MeasurementPlotter.PLOT_IQ):
        """Draws a trace on the canvas."""
        if trace:
            self.trace = trace
        if not self.trace:
            return
        fig = self.fig
        fig.clf()
        self.axes = fig.subplots(nrows=2, ncols=1, sharex=True)
        time, self.timescale, unit = timescale(self.trace.T)
        #self.timescale = np.ceil(np.log10(self.trace.T[1]) / 3) * 3
        #prefixes = {-3: 'ms', -6: u'µs', '-9': 'ns'}
        #unit = prefixes.get(int(self.timescale), 's')
        #time = self.trace.T / np.power(10, self.timescale)
        if plot_type == MeasurementPlotter.PLOT_IQ:
            signal1 = np.real(self.trace.Z)
            signal2 = np.imag(self.trace.Z)
            ylabel1 = 'In-phase (V)'
            ylabel2 = 'Quadrature (V)'
            ylim1 = ylim2 = np.array((min(signal1.min(), signal2.min()),
                                      max(signal1.max(), signal2.max())))*1.1
        elif plot_type == MeasurementPlotter.PLOT_MAG:
            signal1 = np.abs(self.trace.Z)
            signal2 = np.rad2deg(np.unwrap(np.angle(self.trace.Z)))
            ylabel1 = 'Magnitude (V)'
            ylabel2 = 'Phase (deg)'
            ylim1 = np.array((signal1.min(), signal1.max()))*1.1
            ylim2 = np.array((signal2.min(), signal2.max()))*1.1
        else:
            raise ValueError("Unknown plot_type: {}".format(plot_type))
        self.axes[0].plot(time, signal1, lw=0.8)
        self.axes[1].plot(time, signal2, lw=0.8)
        if self.roi:
            self._redrawRoiLine(self.startLine, time[self.roi[0]])
            self._redrawRoiLine(self.stopLine,  time[self.roi[1]])
        else:
            self.startLine = self.stopLine = None
        self.axes[1].set_xlabel('Time ({:s})'.format(unit))
        self.axes[0].set_ylabel(ylabel1)
        self.axes[1].set_ylabel(ylabel2)
        self.axes[0].set_ylim(ylim1)
        self.axes[1].set_ylim(ylim2)
        self.redraw()


class MplNavigationToolbar(NavigationToolbar):
    toolitems = [t for t in NavigationToolbar.toolitems if
                 t[0] in ('Home', 'Pan', 'Zoom', 'Save')]


class TraceSelector(QtWidgets.QWidget):
    valueChanged = QtCore.pyqtSignal(int)

    def __init__(self, sweepvars, parent=None, fmt='{}'):
        super().__init__(parent)

        ui_file = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               'ui', 'sweep_slider.ui')
        loadUi(ui_file, self)

        self.sweepvars = sweepvars
        self.fmt = fmt
        self.sliderSweepIndex.setMinimum(0)
        self.sliderSweepIndex.setMaximum(len(sweepvars) - 1)
        self.sliderSweepIndex.valueChanged.connect(self.onValueChange)
        self.onValueChange(self.sliderSweepIndex.value())

    def onValueChange(self, index):
        self.labelSweepValue.setText(self.fmt.format(self.sweepvars[index]))
        self.valueChanged.emit(index)

    def sweepIndex(self):
        return self.sliderSweepIndex.value()

    def setSweepIndex(self, index):
        self.sliderSweepIndex.setValue(index)

    def sweepValue(self):
        return self.sweepvars[self.sliderSweepIndex.value()]

    def sweepName(self):
        return self.labelSweepName.text()

    def setSweepName(self, name):
        self.labelSweepName.setText(name)
