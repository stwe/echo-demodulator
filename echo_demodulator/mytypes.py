# -*- coding: utf-8 -*-
"""
This file is part of the Echo Demodulator program.
Copyright (c) 2017-2019 by Stefan Weichselbaumer

Echo Demodulator is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Echo Demodulator is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Echo Demodulator.  If not, see <http://www.gnu.org/licenses/>.


This modules contains the PyTables column descriptors."""

import numpy as np
import tables as tb

class DemodulatedTimeTraceInfo(tb.IsDescription):
    fs                = tb.Float64Col()    # Sample rate of original signal
    if_frequency      = tb.Float64Col()    # Intermediate frequency
    apply_lowpass     = tb.BoolCol()       # Lowpass applied or not
    lowpass_frequency = tb.Float64Col()    # Lowpass frequency
    decimate          = tb.BoolCol()       # Signal decimated or not
    decimation_factor = tb.UInt16Col()     # Decimation factor
    decimated_fs      = tb.Float64Col()    # Sample rate of decimated signal
    demod_phase       = tb.Float64Col()    # Demodulation phase
    roi               = tb.Int32Col(shape=(2,)) # Region of interest

def from_trace(trace):
    """Creates a description from a given trace."""
    index = np.asarray(trace.index)
    cls = DemodulatedTimeTraceInfo
    cls.columns['index'] = tb.Int32Col(shape=index.shape)
    cls.columns['T']     = tb.Float64Col(shape=trace.T.shape)
    cls.columns['Z']     = tb.ComplexCol(16, shape=trace.Z.shape)
    maxlength = max([len(s) for s in trace.sweepvars.keys()])
    shape = (len(trace.sweepvars),)
    cls.columns['sweepvars'] = tb.StringCol(maxlength, shape=shape)
    cls.columns['sweepvalues'] = tb.Float64Col(shape=shape)

    return cls


if __name__ == '__main__':
    import numpy as np
    a = DemodulatedTimeTraceInfo
    col = tb.Int32Col(shape=(2,))
    a.columns['index'] = col
    print(a.columns)
