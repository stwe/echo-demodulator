# -*- coding: utf-8 -*-
"""
This file is part of the Echo Demodulator program.
Copyright (c) 2017-2019 by Stefan Weichselbaumer

Echo Demodulator is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Echo Demodulator is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Echo Demodulator.  If not, see <http://www.gnu.org/licenses/>.


This module contains various signal processing methods.
"""
import numpy as np
from scipy import signal

def timescale(time):
    """Returns a rescaled time array, the scaling factor and a corresponding SI
    prefix"""
    time = np.asarray(time)
    if np.count_nonzero(time) == 0:
        return 0, 0, 's'
    timescale = np.floor(np.log10(time.max()) / 3) * 3
    prefixes = {-3: 'ms', -6: u'µs', '-9': 'ns'}
    unit = prefixes.get(int(timescale), 's')
    time = time / np.power(10, timescale)
    return time, timescale, unit

def bandpass(Z, lowcut, highcut, fs, order=5):
    """Apply a Butterworth bandpass filter to Z.
    
    Params
    ======
        Z : the signal
        lowcut: lower cutoff frequency
        highcut: upper cutoff frequency
        fs: sampling rate
        order: order of the filter
    """
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = signal.butter(order, [low, high], btype='band', analog=False)
    return signal.filtfilt(b, a, Z)

def lowpass(Z, cutoff, fs, order=5):
    """Apply a Butterworth lowpass filter to Z.

    Params
    ======
        Z: the signal
        cutoff: the cutoff frequency
        fs: sampling rate
        order: order of the filter
    """
    nyq = 0.5*fs
    cutoff = cutoff / nyq
    b, a = signal.butter(order, cutoff, btype='low', analog=False)
    #return signal.filtfilt(b, a, Z)
    return signal.lfilter(b, a, Z)

