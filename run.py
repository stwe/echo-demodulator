import sys

from PyQt5 import QtWidgets
from echo_demodulator.main import MainWindow

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    mw = MainWindow()
    mw.show()
    mw.onLoadTdmsFile()

    sys.exit(app.exec_())

